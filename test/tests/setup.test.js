describe('Test environment is defined', () => {
    it('has the public frontend url', () => {
        expect(PUBLIC_FRONTEND_URL).toContain('/');
    });

    it('has the public backend url', () => {
        expect(PUBLIC_BACKEND_URL).toContain('/');
    });

    it('has the backoffice url', () => {
        expect(BACKOFFICE_URL).toContain('/');
    });
});
