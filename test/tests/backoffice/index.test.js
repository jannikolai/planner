const puppeteer = require('puppeteer');
const jestscreenshot = require('@jeeyah/jestscreenshot');
const path = require('path');

describe('Backoffice', () => {
    let browser = null;
    let page = null;

    beforeAll(async () => {
        browser = await puppeteer.launch(LAUNCH_OPTIONS);
        page = await browser.newPage();

        await jestscreenshot.init({
            page: page,
            dirName: __dirname,
            scriptName: path.basename(__filename).replace('.js', ''),
        });
    });

    it('is up', async () => {
        await page.goto(BACKOFFICE_URL);
        await page.waitForSelector('h1');
        const html = await page.$eval('h1', e => e.innerHTML);
        expect(html).toContain('Account Login');
    }, TIMEOUT);

    it('allows login', async () => {
        await page.goto(BACKOFFICE_URL);
        await page.type('#id_username', BACKOFFICE_ADMIN_USERNAME);
        await page.type('#id_password', BACKOFFICE_ADMIN_PASSWORD);
        await Promise.all([
            page.waitForNavigation(),
            page.click('button[type="submit"]'),
        ]);
        await page.waitForSelector('h1');
        const html = await page.$eval('h1', e => e.innerHTML);
        expect(html).toBe(`Willkommen zurück ${BACKOFFICE_ADMIN_USERNAME}`);
    }, TIMEOUT);

    afterAll(() => {
        jestscreenshot.cleanup(function () {
            if (browser) {
                browser.close();
            }
        });
    });
});
