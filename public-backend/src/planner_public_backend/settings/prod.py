import email.utils

from .base import *

DEBUG = False

assert MIDDLEWARE[0].endswith(
    ".SecurityMiddleware"
), "MIDDLEWARE setup isn't as expected"

MIDDLEWARE = [
    MIDDLEWARE[0],
    "whitenoise.middleware.WhiteNoiseMiddleware",
    *MIDDLEWARE[1:],
]


# Must contain a list of all public hostnames for the service as a comma-separated list.
# See https://docs.djangoproject.com/en/3.0/ref/settings/#allowed-hosts
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS")

SECRET_KEY = env.str("SECRET_KEY")

STATICFILES_DIRS = ["/tmp/public-client/dist"]
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"
STATIC_ROOT = PROJECT_DIR("collected_static")

WEBPACK_LOADER = {
    "DEFAULT": {
        **WEBPACK_LOADER_DEFAULT_DEFAULTS,
        "CACHE": True,
        "STATS_FILE": "/tmp/public-client/webpack-stats.json",
    }
}

# EMAIL_URL="smtp://user:password@hostname:25". See EMAIL_SCHEMES at
# https://github.com/joke2k/django-environ/blob/master/environ/environ.py for more
# details,
vars().update(env.email_url("EMAIL_URL"))

# ADMINS=Full Name <email-with-name@example.com>,anotheremailwithoutname@example.com
ADMINS = email.utils.getaddresses([env("ADMINS")])

SERVER_EMAIL = env("ERROR_EMAIL_FROM")
DEFAULT_FROM_EMAIL = env("DEFAULT_EMAIL_FROM")

DATABASES = {"default": env.db()}

PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = env.str("PLANNER_PUBLIC_BACKEND_AUTH_TOKEN")
