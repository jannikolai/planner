import dataclasses
import json
from datetime import timezone
from enum import Enum

from factory import DjangoModelFactory, Factory, Faker, LazyAttribute

from ..models import Application, Gender


class ApplicationFactory(DjangoModelFactory):
    class Meta:
        model = Application

    class Params:
        first_name = Faker("first_name")
        last_name = Faker("last_name")
        email = Faker("email")
        phone_number = Faker("phone_number")
        date_of_birth = Faker("date", pattern="%Y-%m-%d")
        gender = Faker("random_element", elements=Gender.values)
        # @todo make this use faker
        nationalities = ["de"]
        spoken_languages = [{"language": "deu", "points": 3}]
        motivation = Faker("paragraph")
        qualification = Faker("paragraph")

    data = LazyAttribute(
        lambda o: {
            "first_name": o.first_name,
            "last_name": o.last_name,
            "email": o.email,
            "phone_number": o.phone_number,
            "date_of_birth": o.date_of_birth,
            "gender": o.gender,
            "nationalities": o.nationalities,
            "spoken_languages": o.spoken_languages,
            "motivation": o.motivation,
            "qualification": o.qualification,
        }
    )
    created = Faker("past_datetime", tzinfo=timezone.utc)


@dataclasses.dataclass
class ApplicationData:
    first_name: str
    last_name: str
    email: str
    phone_number: str
    date_of_birth: str
    gender: str
    nationalities: list
    spoken_languages: list
    motivation: str
    qualification: str
    phone_number: str
    positions: list

    @staticmethod
    def valid_positions():
        return [
            "master",
            "1st-officer",
            "2nd-officer",
            "chief-eng",
            "2nd-3rd-eng",
            "ship-elec-it",
            "bosun",
            "ab-seaman",
            "guest-coord",
            "cook",
            "rib-driver",
            "medic",
            "cult-coord",
            "media-coord",
            "hom",
        ]

    def as_json(self):
        data = dataclasses.asdict(self)
        return json.dumps(data)


class ApplicationDataFactory(Factory):
    class Meta:
        model = ApplicationData

    first_name = Faker("first_name")
    last_name = Faker("last_name")
    email = Faker("email")
    phone_number = Faker("phone_number")
    date_of_birth = Faker("date", pattern="%Y-%m-%d")
    gender = Faker("random_element", elements=Gender.values)
    positions = Faker(
        "random_elements", elements=ApplicationData.valid_positions(), unique=True
    )
    # @todo make this use faker
    nationalities = ["de"]
    spoken_languages = [{"language": "de", "points": 5}]
    motivation = Faker("paragraph")
    qualification = Faker("paragraph")
    phone_number = Faker("bothify", text="+############")
