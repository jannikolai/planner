import pytest
from django.http import HttpResponseForbidden
from django.test import RequestFactory, override_settings

from ..decorators import requires_token_authentication


@requires_token_authentication
def mock_failing_view(request):
    pytest.fail("View has been called even though auth token was not provided")


@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_requires_token_authentication(rf: RequestFactory):
    request = rf.get("/")
    response = mock_failing_view(request)
    assert isinstance(response, HttpResponseForbidden)


@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_requires_token_rejects_requests_with_invalid_token(rf: RequestFactory):
    request = rf.get("/", HTTP_AUTHORIZATION="Token THIS_IS_NOT_A_VALID_TOKEN")
    response = mock_failing_view(request)
    assert isinstance(response, HttpResponseForbidden)


@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_requires_valid_token_authentication(rf: RequestFactory):
    success = object()

    @requires_token_authentication
    def mock_success_view(request):
        return success

    request = rf.get("/", HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN")
    response = mock_success_view(request)
    assert response is success
