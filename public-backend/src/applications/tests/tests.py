import json
import os
from datetime import datetime, timedelta, timezone
from uuid import uuid4

import dateutil.parser
import pytest
from django.conf import settings
from django.core import mail
from django.http.response import HttpResponseForbidden, HttpResponseNotAllowed
from django.shortcuts import reverse
from django.test import Client, RequestFactory, override_settings

from .. import views
from ..models import Application, Language
from .factories import ApplicationData, ApplicationDataFactory, ApplicationFactory

with open(
    os.path.join(settings.COMMON_DIR, "schema", "initialApplication.json")
) as json_file:
    schema = json.load(json_file)


@pytest.mark.django_db
def test_application_create_is_reachable(client: Client):
    response = client.post(
        reverse("application_create"),
        ApplicationDataFactory().as_json(),
        content_type="application/json",
    )
    assert response.status_code == 200


def test_application_create_raises_get_not_allowed(rf: RequestFactory):
    request = rf.get("application_create")
    response = views.application_create(request)
    assert isinstance(response, HttpResponseNotAllowed)


@pytest.mark.django_db
def test_application_create(rf: RequestFactory):
    alice = ApplicationDataFactory()

    request = rf.post(
        "application_create", alice.as_json(), content_type="application/json",
    )
    response = views.application_create(request)
    assert response.status_code == 200

    application = Application.objects.last()

    assert application.data["first_name"] == alice.first_name
    assert application.data["last_name"] == alice.last_name
    assert application.data["email"] == alice.email
    assert application.data["date_of_birth"] == alice.date_of_birth
    assert application.data["gender"] == alice.gender
    assert application.data["motivation"] == alice.motivation
    assert application.data["qualification"] == alice.qualification
    assert application.data["phone_number"] == alice.phone_number
    assert application.data["positions"] == alice.positions

    assert len(application.data["spoken_languages"]) == 1
    assert (
        application.data["spoken_languages"][0]["language"]
        == alice.spoken_languages[0]["language"]
    )
    assert (
        application.data["spoken_languages"][0]["points"]
        == alice.spoken_languages[0]["points"]
    )


@pytest.mark.django_db
def test_to_reject_invalid_positions(rf: RequestFactory):
    alice = ApplicationDataFactory(positions=["invalid"])

    request = rf.post(
        "application_create", alice.as_json(), content_type="application/json",
    )
    response = views.application_create(request)
    assert response.status_code == 422

    assert Application.objects.count() == 0


@pytest.mark.django_db
def test_to_reject_duplicate_positions(rf: RequestFactory):
    alice = ApplicationDataFactory(
        positions=[
            ApplicationData.valid_positions()[0],
            ApplicationData.valid_positions()[0],
        ]
    )

    request = rf.post(
        "application_create", alice.as_json(), content_type="application/json",
    )
    response = views.application_create(request)
    assert response.status_code == 422

    assert Application.objects.count() == 0


@pytest.mark.django_db
def test_to_save_nationalities(rf: RequestFactory):
    expected_nationalities = ["DE", "GB"]
    alice = ApplicationDataFactory(nationalities=expected_nationalities)

    request = rf.post(
        "application_create", alice.as_json(), content_type="application/json",
    )
    response = views.application_create(request)
    assert response.status_code == 200

    assert Application.objects.count() == 1
    assert Application.objects.last().data["nationalities"] == expected_nationalities


@pytest.mark.django_db
def test_application_create_fails_with_missing_fields(rf: RequestFactory):
    request = rf.post("application_create", {}, content_type="application/json")
    response = views.application_create(request)
    assert response.status_code == 422
    assert json.loads(response.content) == {
        "ok": False,
        "message": "'first_name' is a required property",
    }


minimal_application = {
    "first_name": "Alice",
    "last_name": "Turing",
    "gender": "none_other",
    "email": "alice@example.net",
    "date_of_birth": "1999-09-09",
    "phone_number": "+491984772",
    "motivation": "a",
    "qualification": "b",
    "nationalities": ["deu"],
    "spoken_languages": [{"language": "de", "points": 3}],
}


@pytest.mark.django_db
def test_minimal_application_is_accepted(rf: RequestFactory):
    # test if minimal application is complete
    request = rf.post(
        "application_create", minimal_application, content_type="application/json",
    )
    assert views.application_create(request).status_code == 200


@pytest.mark.django_db
def test_application_create_raises_without_mandatory_fields(rf: RequestFactory):
    for key in minimal_application:
        field_value = minimal_application[key]
        application_with_missing_field = minimal_application.copy()
        application_with_missing_field.pop(key)
        request = rf.post(
            "application_create",
            application_with_missing_field,
            content_type="application/json",
        )
        assert (
            views.application_create(request).status_code == 422
        ), f"Application was created with missing field {key}"


@pytest.mark.django_db
def test_application_create_raises_with_additional_properties(rf: RequestFactory):
    application = minimal_application.copy()
    application["x"] = 666
    request = rf.post(
        "application_create", application, content_type="application/json",
    )
    assert views.application_create(request).status_code == 422
    assert (
        json.loads(views.application_create(request).content)["message"]
        == "Additional properties are not allowed ('x' was unexpected)"
    )

    application = minimal_application.copy()
    application["spoken_languages"] = [{"language": "deu", "points": 5, "y": 6}]
    request = rf.post(
        "application_create", application, content_type="application/json",
    )
    assert views.application_create(request).status_code == 422
    assert (
        json.loads(views.application_create(request).content)["message"]
        == "Additional properties are not allowed ('y' was unexpected)"
    )


def test_schema_defines_denial_of_all_possible_addition_properties():
    assert schema["additionalProperties"] == False

    # same check recursively
    def recursively_check(node):
        if node["type"] == "array":
            recursively_check(node["items"])
        elif node["type"] == "object":
            assert node["additionalProperties"] == False
            for prop in node["properties"].values():
                recursively_check(prop)

    recursively_check(schema)


@pytest.mark.django_db
def test_application_create_raises_with_future_date_of_birth(rf: RequestFactory):
    tomorrow = datetime.now() + timedelta(days=1)
    application = minimal_application.copy()
    application[
        "date_of_birth"
    ] = f"{tomorrow.year}-{'0' if tomorrow.month < 10 else ''}{tomorrow.month}-{'0' if tomorrow.day < 10 else ''}{tomorrow.day}"

    request = rf.post(
        "application_create", application, content_type="application/json",
    )
    assert views.application_create(request).status_code == 422


@pytest.mark.django_db
def test_application_create_raises_with_invalid_date_of_birth(rf: RequestFactory):
    application = minimal_application.copy()
    application["date_of_birth"] = "abcd-ef-gh"

    request = rf.post(
        "application_create", application, content_type="application/json",
    )
    assert views.application_create(request).status_code == 422


@pytest.mark.django_db
def test_appliation_create_doesnt_require_csrf_token():
    client = Client(enforce_csrf_checks=True)
    response = client.post(
        reverse("application_create"),
        ApplicationDataFactory().as_json(),
        content_type="application/json",
    )
    assert not isinstance(response, HttpResponseForbidden)


def test_form_specification_is_reachable(client: Client):
    response = client.get(reverse("form_specification"))

    assert response.status_code == 200


def test_form_specification_data(rf: RequestFactory):
    request = rf.get("form_specification")
    response = views.form_specification(request)

    assert json.loads(response.content) == {
        "ok": True,
        "templateName": "initial",
        "formData": {"first_name": "", "phone_number": ""},
    }


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_detail_oldest_is_reachable(client: Client):
    response = client.get(
        reverse("application_detail_oldest"),
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    assert response.status_code == 204


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_detail_oldest_returns_oldest_application(rf: RequestFactory):
    first_name = "Old Application"
    uid = "some uuid"
    phone_number = "+491739948582"
    test_datetime = datetime(2020, 1, 12, 3, 0, 0, tzinfo=timezone.utc)
    ApplicationFactory(
        first_name=first_name,
        phone_number=phone_number,
        uid=uid,
        created=test_datetime,
    )
    ApplicationFactory()
    request = rf.get(
        "application_detail_oldest", HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN"
    )
    response = views.application_detail_oldest(request)
    application = json.loads(response.content)
    application_created_datetime = dateutil.parser.isoparse(application["created"])

    # We only test fields that are already imported by the backoffice
    assert application["data"]["first_name"] == first_name
    assert application["data"]["phone_number"] == phone_number
    assert application["uid"] == uid
    assert application_created_datetime == test_datetime


@pytest.mark.django_db
def test_application_details_oldest_requires_token(rf: RequestFactory):
    request = rf.delete("application_detail_oldest")
    response = views.application_detail_oldest(request)
    assert isinstance(response, HttpResponseForbidden)


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_delete_is_reachable(client: Client):
    response = client.delete(
        reverse("application_delete", kwargs={"uid": uuid4()}),
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    assert response.status_code == 404


@pytest.mark.django_db
@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_delete_deletes_application_in_db(rf: RequestFactory):
    uid = str(uuid4())
    ApplicationFactory(uid=uid)
    request = rf.delete(
        "application_delete", HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN"
    )
    response = views.application_delete(request, uid=uid)
    assert response.status_code == 200
    assert json.loads(response.content)["ok"]
    assert Application.objects.filter(uid=uid).count() == 0


def test_application_delete_requires_token(rf: RequestFactory):
    request = rf.delete("application_delete")
    response = views.application_delete(request, uid="asldalks")
    assert isinstance(response, HttpResponseForbidden)


@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_request_availabilities_is_reachable(client: Client):
    response = client.get(
        reverse("application_request_availabilities"),
        data={"email": "test@sea-watch.org"},
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    assert response.status_code == 200


@pytest.mark.django_db
def test_application_request_availabilities_requires_token(rf: RequestFactory):
    request = rf.get("application_request_availabilities")
    response = views.application_request_availabilities(request)
    assert isinstance(response, HttpResponseForbidden)


@override_settings(PLANNER_PUBLIC_BACKEND_AUTH_TOKEN="THIS_IS_A_VALID_TOKEN")
def test_application_request_availabilities_sends_email(rf: RequestFactory):
    mailcount_before = len(mail.outbox)
    request = rf.get(
        "application_request_availabilities",
        data={"email": "test@sea-watch.org"},
        HTTP_AUTHORIZATION="Token THIS_IS_A_VALID_TOKEN",
    )
    response = views.application_request_availabilities(request)
    mailcount_after = len(mail.outbox)
    assert mailcount_after == mailcount_before + 1
    app_request_email = mail.outbox[mailcount_before]
    assert "test@sea-watch.org" in app_request_email.to
    assert settings.DEFAULT_FROM_EMAIL == app_request_email.from_email
    message = "We need to know if you are available for upcoming missions"
    assert app_request_email.subject == "Mission availability request"
    assert message == app_request_email.body
