# Contribution/Dev documentation

This needs some content :(

## Pre-Commit checks

We're using pre-commit to ensure we don't commit preventable errors. To set it
up, you need to follow a few steps:

1. Create a virtual environment using either `python -m venv env` or direnv
2. Install the dev requirements: `pip install -r requirements-dev.txt`
    NB: pre-commit is part of all requirements-dev.txt files, so people using
    direnv can commit from all directories.
3. Run `pre-commit install` to set up the git hooks. This needs to be done once
   per git clone.
