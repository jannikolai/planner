# Project management
pip-tools~=5.2

# Formatting and checking
pre-commit~=2.5
black==19.10b0
isort[requirements_deprecated_finder]~=5.0

# Better debugger
pdbpp~=0.10

# Tests
factory_boy~=2.12
faker~=4.1.1
pytest-django~=3.9
pytest-testmon~=1.0
pytest-watch~=4.2
pytest-httpserver~=0.3.5
