from datetime import date

from django.http import HttpResponseForbidden
from pytest_django.asserts import assertRedirects

from seawatch_registration.tests.factories import UserFactory


def assert_redirects_to_login(url, client):
    assertRedirects(client.get(url), f"/accounts/login/?next={url}")


def assert_requires_permission(url, permission, client):
    user = UserFactory()

    client.force_login(user)
    response = client.get(url, user=user)
    assert type(response) is HttpResponseForbidden
