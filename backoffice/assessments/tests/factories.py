import factory

from seawatch_registration.tests.factories import ProfileFactory

from ..models import Assessment


class AssessmentFactory(factory.DjangoModelFactory):
    class Meta:
        model = Assessment

    profile = factory.SubFactory(ProfileFactory)
