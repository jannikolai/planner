from datetime import date

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, TestCase
from django.urls import reverse
from django.utils.formats import date_format

from assessments.models import Assessment
from assessments.tests import base
from assessments.tests.factories import AssessmentFactory
from seawatch_registration.models import (
    Answer,
    Document,
    DocumentType,
    Position,
    Question,
    Skill,
)
from seawatch_registration.tests.factories import PositionFactory, UserFactory

from ..factories import ProfileFactory


class TestAssessmentView(TestCase):
    def setUp(self):
        self.client = Client()
        self.url = reverse("assessment_update", kwargs={"pk": 1})

    def test_requires_authentication(self):
        base.assert_redirects_to_login(
            reverse("assessment_update", kwargs={"pk": 1}), self.client
        )

    def test_permission_denied_on_missing_permission(self):
        base.assert_requires_permission(
            reverse("assessment_update", kwargs={"pk": 1}),
            "can_assess_profiles",
            self.client,
        )

    def test_get__should_return_404_when_not_existing_profil_id_is_given(self):
        # Arrange
        user = UserFactory(permission="can_assess_profiles")
        self.client.force_login(user)

        # Act
        response = self.client.get(reverse("assessment_update", kwargs={"pk": 123}))

        # Assert
        self.assertEqual(response.status_code, 404)

    def test_get__should_return_404_when_profil_id_exists_but_has_no_assessment(self):
        # Arrange
        ProfileFactory(id=91234)

        # Act
        self.client.force_login(UserFactory(permission="can_assess_profiles"))
        response = self.client.get(reverse("assessment_update", kwargs={"pk": 91234}))

        # Assert
        self.assertEqual(response.status_code, 404)

    def test_get__should_show_not_specified_when_data_is_missing(self):
        # Arrange
        profile = ProfileFactory(citizenship=["DE", "US"])
        assessment = Assessment(profile=profile, status=Assessment.PENDING)
        assessment.save()

        # Act
        self.client.force_login(UserFactory(permission="can_assess_profiles"))
        response = self.client.get(
            reverse("assessment_update", kwargs={"pk": assessment.pk})
        )

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "assessment-update.html")
        self.assertContains(
            response, '<dd class="col-sm-6 text-danger">Not specified</dd>'
        )
        self.assertContains(response, '<dd class="col-sm-6">German, American</dd>')
        self.assertContains(response, p_danger("No skills specified."))
        self.assertContains(response, p_danger("No requested positions specified."))
        self.assertContains(response, p_danger("No Documents uploaded."))
        self.assertContains(response, p_danger("No questions answered."))

    def test_get__should_show_data_when_all_data_is_set(self):
        # Arrange
        profile = ProfileFactory(citizenship=["DE", "US"])

        assessment = Assessment(profile=profile, status=Assessment.PENDING)
        assessment.save()
        question = Question.objects.all().first()
        answer1 = Answer(question=question, profile=profile, text="Test Answer 1")
        answer2 = Answer(question=question, profile=profile, text="Test Answer 2")
        answer1.save()
        answer2.save()
        skill1 = Skill.objects.all().first()
        skill2 = Skill.objects.all().last()
        profile.skills.set([skill1, skill2])
        position1 = Position.objects.all().first()
        position2 = Position.objects.all().last()
        profile.requested_positions.set([position1, position2])
        document_type = DocumentType(name="Passport")
        document_type.save()
        document = Document(
            document_type=document_type,
            profile=profile,
            number="1234",
            issuing_date=date.today(),
            expiry_date=date.today(),
            issuing_authority="NSA",
            issuing_place="Crypto City",
            issuing_country="United States of America",
            file=SimpleUploadedFile(
                "testfile.jpg", b"file_content", content_type="image/jpg"
            ),
        )
        document.save()

        # Act
        self.client.force_login(UserFactory(permission="can_assess_profiles"))
        response = self.client.get(
            reverse("assessment_update", kwargs={"pk": assessment.pk})
        )

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "assessment-update.html")
        self.assertContains(
            response, '<dd class="col-sm-6 text-danger">Not specified</dd>'
        )
        self.assertContains(response, '<dd class="col-sm-6">German, American</dd>')
        self.assertContains(response, li(skill1.name))
        self.assertContains(response, li(skill2.name))
        self.assertContains(response, li(position1.name))
        self.assertContains(response, li(position2.name))
        self.assertContains(response, '<dd class="col-sm-8">Test Answer 1</dd>')
        self.assertContains(response, '<dd class="col-sm-8">Test Answer 2</dd>')
        self.assertContains(response, td(document.pk))
        self.assertContains(response, td(document.document_type))
        self.assertContains(response, td(document.number))
        self.assertContains(
            response, td(date_format(document.issuing_date, "DATE_FORMAT"))
        )
        self.assertContains(
            response, td(date_format(document.expiry_date, "DATE_FORMAT"))
        )
        self.assertContains(response, td(document.issuing_authority))
        self.assertContains(response, td(document.issuing_place))
        self.assertContains(response, td(document.issuing_country))
        self.assertContains(response, td(document.file.name))

    def test_get__should_set_initial_values_when_values_are_set(self):
        # Arrange
        position_id = 19134
        position = PositionFactory(id=position_id, name="42nd mate")
        profile = ProfileFactory(
            citizenship=["DE", "US"],
            requested_positions=[position],
            approved_positions=[position],
        )
        assessment = AssessmentFactory(
            profile=profile, status=Assessment.PENDING, comment="Test Comment",
        )

        # Act
        self.client.force_login(UserFactory(permission="can_assess_profiles"))
        response = self.client.get(
            reverse("assessment_update", kwargs={"pk": assessment.pk})
        )

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "assessment-update.html")
        self.assertContains(response, '<dd class="col-sm-6">German, American</dd>')
        self.assertContains(response, p_danger("No skills specified."))
        self.assertContains(response, p_danger("No Documents uploaded."))
        self.assertContains(response, p_danger("No questions answered."))
        self.assertContains(
            response,
            checked_required_radio_input(name="status", value=Assessment.PENDING),
        )
        self.assertContains(response, selected_option_(position_id, "42nd mate"))
        self.assertContains(response, "Test Comment</textarea>")

    def test_post__should_return_404_when_not_existing_profil_id_is_given(self):
        # Act
        self.client.force_login(UserFactory(permission="can_assess_profiles"))
        response = self.client.post(
            reverse("assessment_update", kwargs={"pk": 123}),
            {"approved_positions": 1, "status": Assessment.ACCEPTED, "comment": ""},
        )

        # Assert
        self.assertEqual(response.status_code, 404)

    def test_post__should_show_not_specified_when_data_is_missing(self):
        # Arrange
        assessment_id = 172
        id_of_position_which_doesnt_exist = 173

        AssessmentFactory(
            id=assessment_id,
            status=Assessment.PENDING,
            profile__citizenship=["DE", "US"],
        )

        # Act
        self.client.force_login(UserFactory(permission="can_assess_profiles"))
        response = self.client.post(
            reverse("assessment_update", kwargs={"pk": assessment_id}),
            {
                ## It is not very clear why approved positions must be an id which does
                ## not exist. Hence we left the comment
                "approved_positions": id_of_position_which_doesnt_exist,  # A bit of a hack
                "status": Assessment.ACCEPTED,
                "comment": "",
            },
        )

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "assessment-update.html")
        self.assertContains(
            response, '<dd class="col-sm-6 text-danger">Not specified</dd>'
        )
        self.assertContains(response, '<dd class="col-sm-6">German, American</dd>')
        self.assertContains(response, p_danger("No skills specified."))
        self.assertContains(response, p_danger("No requested positions specified."))
        self.assertContains(response, p_danger("No Documents uploaded."))
        self.assertContains(response, p_danger("No questions answered."))

    def test_post__should_show_data_when_all_data_is_set(self):
        # Arrange
        profile = ProfileFactory(citizenship=["DE", "US"])
        assessment = Assessment(profile=profile, status=Assessment.PENDING)
        assessment.save()
        question = Question.objects.all().first()
        answer1 = Answer(question=question, profile=profile, text="Test Answer 1")
        answer2 = Answer(question=question, profile=profile, text="Test Answer 2")
        answer1.save()
        answer2.save()
        skill1 = Skill.objects.all().first()
        skill2 = Skill.objects.all().last()
        profile.skills.set([skill1, skill2])
        position1 = Position.objects.all().first()
        position2 = Position.objects.all().last()
        profile.requested_positions.set([position1, position2])
        document_type = DocumentType(name="Passport")
        document_type.save()
        document = Document(
            document_type=document_type,
            profile=profile,
            number="1234",
            issuing_date=date.today(),
            expiry_date=date.today(),
            issuing_authority="NSA",
            issuing_place="Crypto City",
            issuing_country="United States of America",
            file=SimpleUploadedFile(
                "testfile.jpg", b"file_content", content_type="image/jpg"
            ),
        )
        document.save()

        # Act
        self.client.force_login(UserFactory(permission="can_assess_profiles"))
        response_post = self.client.post(
            reverse("assessment_update", kwargs={"pk": assessment.pk}),
            {
                "approved_positions": position1.pk,
                "status": Assessment.ACCEPTED,
                "comment": "",
            },
        )
        response = self.client.get(
            reverse("assessment_update", kwargs={"pk": assessment.pk})
        )

        # Assert
        self.assertEqual(response_post.status_code, 302)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "assessment-update.html")
        self.assertContains(
            response, '<dd class="col-sm-6 text-danger">Not specified</dd>'
        )
        self.assertContains(response, '<dd class="col-sm-6">German, American</dd>')
        self.assertContains(response, li(skill1.name))
        self.assertContains(response, li(skill2.name))
        self.assertContains(response, li(position1.name))
        self.assertContains(response, li(position2.name))
        self.assertContains(response, '<dd class="col-sm-8">Test Answer 1</dd>')
        self.assertContains(response, '<dd class="col-sm-8">Test Answer 2</dd>')
        self.assertContains(response, td(document.pk))
        self.assertContains(response, td(document.document_type))
        self.assertContains(response, td(document.number))
        self.assertContains(
            response, td(date_format(document.issuing_date, "DATE_FORMAT"))
        )
        self.assertContains(
            response, td(date_format(document.expiry_date, "DATE_FORMAT"))
        )
        self.assertContains(response, td(document.issuing_authority))
        self.assertContains(response, td(document.issuing_place))
        self.assertContains(response, td(document.issuing_country))
        self.assertContains(response, td(document.file.name))

    def test_post__should_save_new_values_when_values_are_set(self):
        # Arrange
        profile = ProfileFactory()
        assessment = Assessment(profile=profile, status=Assessment.PENDING)
        assessment.save()
        position1 = Position.objects.all().first()
        position2 = Position.objects.all().last()
        profile.requested_positions.set([position1, position2])
        profile.approved_positions.set([position1])

        # Act
        self.client.force_login(UserFactory(permission="can_assess_profiles"))
        response_post = self.client.post(
            reverse("assessment_update", kwargs={"pk": assessment.pk}),
            {
                "approved_positions": position2.pk,
                "status": Assessment.ACCEPTED,
                "comment": "Test Comment",
            },
        )
        response = self.client.get(
            reverse("assessment_update", kwargs={"pk": assessment.pk})
        )

        # Assert
        self.assertEqual(response_post.status_code, 302)
        self.assertTemplateUsed(response, "assessment-update.html")
        self.assertContains(response, selected_option(position2))
        self.assertEqual(
            Assessment.objects.get(profile=profile).status, Assessment.ACCEPTED
        )
        self.assertEqual(
            Assessment.objects.get(profile=profile).comment, "Test Comment"
        )
        self.assertEqual(profile.approved_positions.all().first(), position2)
        self.assertEqual(profile.approved_positions.all().count(), 1)

    def test_post__should_delete_approved_positions_when_no_positions_are_set(self):
        # Arrange
        profile = ProfileFactory()
        assessment = Assessment(
            profile=profile, status=Assessment.PENDING, comment="Test Comment"
        )
        assessment.save()
        position1 = Position.objects.all().first()
        position2 = Position.objects.all().last()
        profile.requested_positions.set([position1, position2])
        profile.approved_positions.set([position1, position2])

        # Act
        self.client.force_login(UserFactory(permission="can_assess_profiles"))
        response_post = self.client.post(
            reverse("assessment_update", kwargs={"pk": assessment.pk}),
            {"status": Assessment.ACCEPTED, "comment": ""},
        )
        response = self.client.get(
            reverse("assessment_update", kwargs={"pk": assessment.pk})
        )

        # Assert
        self.assertEqual(response_post.status_code, 302)
        self.assertTemplateUsed(response, "assessment-update.html")
        self.assertEqual(
            Assessment.objects.get(profile=profile).status, Assessment.ACCEPTED
        )
        self.assertEqual(Assessment.objects.get(profile=profile).comment, "")
        self.assertEqual(profile.approved_positions.all().count(), 0)


def checked_required_radio_input(name, value, id_number=0, is_post_request=False):
    if is_post_request:
        class_is_valid = "is-valid "
    else:
        class_is_valid = ""
    return (
        '<input checked="" class="'
        + class_is_valid
        + 'form-check-input" id="id_'
        + name
        + "_"
        + str(id_number)
        + '" name="'
        + name
        + '" required="" title="" type="radio" value="'
        + value
        + '"/>'
    )


def selected_option(position: Position):
    return selected_option_(position.pk, position.name)


def selected_option_(value, option: str):
    return f'<option value="{value}" selected>{option}</option>'


def p_danger(text):
    return '<p class="text-danger">' + text + "</p>"


def li(text):
    return '<li class="list-group-item">' + text + "</li>"


def td(value):
    return "<td>" + str(value) + "</td>"
