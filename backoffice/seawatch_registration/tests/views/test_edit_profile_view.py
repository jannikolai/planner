from datetime import date

from django.test import Client
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertRedirects, assertTemplateUsed

from ...models import Profile
from ..factories import ProfileFactory, UserFactory

url = reverse("profile_update")


@mark.django_db
def test_gets_profile_form_when_profile_for_user_exists(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)

    # Act
    client.force_login(user)
    response = client.get(url)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "./seawatch_registration/profile_update.html")


@mark.django_db
def test_updates_profile_when_profile_for_user_exists(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)

    # Act
    client.force_login(user)
    response = client.post(
        url,
        {
            "first_name": user.first_name,
            "last_name": user.last_name,
            "email": user.email,
            "user": user.id,
            "citizenship": "DE",
            "date_of_birth": date.today(),
            "place_of_birth": "New York",
            "country_of_birth": "US",
            "gender": "m",
            "needs_schengen_visa": False,
            "phone": "0987654321",
        },
    )

    # Assert
    assertRedirects(response, reverse("profile_detail"))
    assert Profile.objects.count() == 1
    assert Profile.objects.first().phone == "0987654321"


@mark.django_db
def test_doesnt_update_profile_when_required_data_is_missing(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user, phone="0123456789")

    # Act
    client.force_login(user)
    response = client.post(url, {"user": user.id, "citizenship": "Deutsch"})

    # Assert
    assert response.status_code == 200
    assert Profile.objects.count() == 1
    assert Profile.objects.first().phone == "0123456789"
