from django.test import Client, RequestFactory
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertTemplateUsed

from ...views.profile import DetailView
from ..asserts import assert_requires_profile
from ..factories import ProfileFactory, UserFactory


@mark.django_db
def test_requires_profile(rf: RequestFactory):
    assert_requires_profile(rf, DetailView)


@mark.django_db
def test_renders_with_profile_detail_html_when_profile_exists(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)
    client.force_login(user)

    # Act
    response = client.get(reverse("profile_detail"))

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "seawatch_registration/profile_detail.html")
