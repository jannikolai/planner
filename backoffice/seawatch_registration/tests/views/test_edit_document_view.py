import tempfile
from datetime import date

from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, TestCase, override_settings
from django.urls import reverse

from seawatch_registration.models import Document, DocumentType, Profile
from seawatch_registration.tests.factories import ProfileFactory, UserFactory


class TestEditDocumentView(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.url = reverse("document_update", kwargs={"document_id": 1})

    def test_views__document_update__get__should_get_403_when_document_doesnt_exist(
        self,
    ):
        # Arrange
        user = UserFactory()
        ProfileFactory(user=user)
        self.client.force_login(user)

        # Act
        response = self.client.get(self.url, user=user)

        # Assert
        self.assertEqual(response.status_code, 403)

    def test_views__document_update__get__should_get_403_when_user_is_not_owner_of_document(
        self,
    ):
        # Arrange
        user = UserFactory()
        ProfileFactory(user=user)

        other_users_profile = ProfileFactory()

        document_type = DocumentType(name="TestType", group="other")
        document_type.save()
        image = SimpleUploadedFile(
            "testfile.jpg", b"file_content", content_type="image/jpg"
        )
        document = Document(
            pk=1,
            profile=other_users_profile,
            document_type=document_type,
            number="123",
            issuing_date=date.today(),
            expiry_date=date.today(),
            issuing_authority="New York City",
            issuing_place="New York City",
            issuing_country="United States of America",
            file=image,
        )
        document.save()

        self.client.force_login(user)

        # Act
        response = self.client.get(self.url, user=user)

        # Assert
        self.assertEqual(response.status_code, 403)

    def test_views__document_update__get__should_render_with_document_html_when_document_exists(
        self,
    ):
        # Arrange
        user = UserFactory()
        profile = ProfileFactory(user=user)

        document_type = DocumentType(name="TestType", group="other")
        document_type.save()
        image = SimpleUploadedFile(
            "testfile.jpg", b"file_content", content_type="image/jpg"
        )
        document = Document(
            pk=1,
            profile=profile,
            document_type=document_type,
            number="123",
            issuing_date=date.today(),
            expiry_date=date.today(),
            issuing_authority="New York City",
            issuing_place="New York City",
            issuing_country="United States of America",
            file=image,
        )
        document.save()

        # Act
        self.client.force_login(user)
        response = self.client.get(self.url, user=user)

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "form.html")
        self.assertContains(response, document.pk)
        self.assertContains(response, document.document_type.name)
        self.assertContains(response, document.number)
        self.assertContains(response, document.issuing_date)
        self.assertContains(response, document.expiry_date)
        self.assertContains(response, document.issuing_authority)
        self.assertContains(response, document.issuing_place)
        self.assertContains(response, document.issuing_country)
        self.assertContains(response, document.file.name)

    def test_views__document_update__post__should_redirect_to_document_list_when_form_is_valid(
        self,
    ):
        # Arrange
        user = UserFactory()
        profile = ProfileFactory(user=user)
        document_type = DocumentType(name="TestType", group="other")
        document_type.save()
        image = SimpleUploadedFile(
            "testfile.jpg", b"file_content", content_type="image/jpg"
        )
        image2 = SimpleUploadedFile(
            "testfile2.jpg", b"file_content_number2", content_type="image/jpg"
        )
        document = Document(
            pk=1,
            profile=profile,
            document_type=document_type,
            number="123",
            issuing_date=date.today(),
            expiry_date=date.today(),
            issuing_authority="New York City",
            issuing_place="New York City",
            issuing_country="United States of America",
            file=image,
        )
        document.save()

        # Act
        self.client.force_login(user)
        response = self.client.post(
            self.url,
            {
                "document_type": document_type.id,
                "number": "1234",
                "issuing_date": date.today(),
                "expiry_date": date.today(),
                "issuing_authority": "New Jersey",
                "issuing_place": "New Jersey",
                "issuing_country": "United States of America",
                "file": image2,
            },
            user=user,
        )

        # Assert
        self.assertRedirects(response, reverse("document_list"))
        self.assertEqual(Document.objects.all().count(), 1)
        new_document = Document.objects.all().first()
        self.assertEqual(new_document.issuing_authority, "New Jersey")
        self.assertEqual(new_document.issuing_place, "New Jersey")

    def test_views__document_update__post__should_render_when_form_is_invalid(self):
        # Arrange
        user = UserFactory()
        profile = ProfileFactory(user=user)

        document_type = DocumentType(name="TestType", group="other")
        document_type.save()
        image = SimpleUploadedFile(
            "testfile.jpg", b"file_content", content_type="image/jpg"
        )
        document = Document(
            pk=1,
            profile=profile,
            document_type=document_type,
            number="123",
            issuing_date=date.today(),
            expiry_date=date.today(),
            issuing_authority="New York City",
            issuing_place="New York City",
            issuing_country="United States of America",
            file=image,
        )
        document.save()

        # Act
        self.client.force_login(user)
        response = self.client.post(
            self.url,
            {"document_type": document_type.id, "issuing_date": "not a date"},
            user=user,
        )

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "form.html")
        self.assertNotContains(response, "alert-success")
        self.assertEqual(Document.objects.all().count(), 1)
        self.assertEqual(Document.objects.all().first(), document)

    def test_views__document_update__post__should_get_403_when_user_is_not_owner_of_document(
        self,
    ):
        # Arrange
        user = UserFactory()
        ProfileFactory(user=user)

        other_users_profile = ProfileFactory()

        document_type = DocumentType(name="TestType", group="other")
        document_type.save()
        image = SimpleUploadedFile(
            "testfile.jpg", b"file_content", content_type="image/jpg"
        )
        image2 = SimpleUploadedFile(
            "testfile2.jpg", b"file_content_number2", content_type="image/jpg"
        )
        document = Document(
            pk=1,
            profile=other_users_profile,
            document_type=document_type,
            number="123",
            issuing_date=date.today(),
            expiry_date=date.today(),
            issuing_authority="New York City",
            issuing_place="New York City",
            issuing_country="United States of America",
            file=image,
        )
        document.save()

        # Act
        self.client.force_login(user)
        response = self.client.post(
            self.url,
            {
                "document_type": document_type.id,
                "number": "1234",
                "issuing_date": date.today(),
                "expiry_date": date.today(),
                "issuing_authority": "New Jersey",
                "issuing_place": "New Jersey",
                "issuing_country": "United States of America",
                "file": image2,
            },
            user=user,
        )

        # Assert
        self.assertEqual(response.status_code, 403)
