from datetime import date

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, RequestFactory
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertNotContains, assertRedirects, assertTemplateUsed

from seawatch_registration.models import Document

from ...views.document import DocumentCreateView
from ..asserts import assert_requires_profile
from ..factories import DocumentTypeFactory, ProfileFactory, UserFactory


@mark.django_db
def test_requires_profile(rf: RequestFactory):
    assert_requires_profile(rf, DocumentCreateView)


url = reverse("document_create")


@mark.django_db
def test_views__document_create__get__should_render_with_document_html_when_profile_exists(
    client: Client,
):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)

    # Act
    client.force_login(user=user)
    response = client.get(url)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")


@mark.django_db
def test_views__document_create__post__should_redirect_to_position_when_form_is_valid(
    client: Client,
):
    # Arrange
    user = UserFactory()
    profile = ProfileFactory(user=user)
    document_type = DocumentTypeFactory()

    image = SimpleUploadedFile(
        "testfile.jpg", b"file_content", content_type="image/jpg"
    )

    # Act
    client.force_login(user)
    response = client.post(
        url + "?initial_registration=yes",
        {
            "document_type": document_type.id,
            "number": "1234",
            "issuing_date": date.today(),
            "expiry_date": date.today(),
            "issuing_authority": "New York City",
            "issuing_city": "New York City",
            "issuing_country": "United States of America",
            "profile": profile.id,
            "file": image,
        },
    )

    # Assert
    assertRedirects(
        response,
        expected_url=f"{reverse('requested_position_update')}?initial_registration=yes",
    )
    assert Document.objects.all().count() == 1


@mark.django_db
def test_views__document_create__post__should_render_when_form_is_invalid(
    client: Client,
):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)
    document_type = DocumentTypeFactory()

    # Act
    client.force_login(user)
    response = client.post(url, {"document_type": document_type.id})

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")
    assertNotContains(response, "alert-success")
    assert Document.objects.all().count() == 0
