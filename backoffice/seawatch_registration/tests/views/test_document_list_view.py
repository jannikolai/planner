import tempfile
from datetime import date

from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import Client, TestCase, override_settings
from django.urls import reverse

from seawatch_registration.models import Document, DocumentType, Position
from seawatch_registration.tests.factories import ProfileFactory, UserFactory


class TestDocumentListView(TestCase):
    def setUp(self):
        self.client = Client()
        self.url = reverse("document_list")

    def test_views__document_list__get__should_show_text_when_no_documents_are_uploaded(
        self,
    ):
        # Arrange
        user = UserFactory()
        ProfileFactory(user=user)

        self.client.force_login(user)

        # Act
        response = self.client.get(self.url, user=user)

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "./seawatch_registration/document_list.html")
        self.assertContains(response, "You didn't upload any documents, yet!")

    def test_views__document_list__get__should_show_table_when_document_is_uploaded(
        self,
    ):
        # Arrange
        user = UserFactory()
        profile = ProfileFactory(user=user)

        document_type = DocumentType(name="TestType", group="other")
        document_type.save()
        image = SimpleUploadedFile(
            "testfile.jpg", b"file_content", content_type="image/jpg"
        )
        document = Document(
            profile=profile,
            document_type=document_type,
            number="123",
            issuing_date=date(2019, 6, 9),
            expiry_date=date(2020, 4, 21),
            issuing_authority="New York City Authority",
            issuing_place="New York City",
            issuing_country="United States of America",
            file=image,
        )
        document.save()

        self.client.force_login(user)

        # Act
        response = self.client.get(self.url, user=user)

        # Assert
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "./seawatch_registration/document_list.html")
        self.assertContains(response, td("TestType"))
        self.assertContains(response, td("123"))
        self.assertContains(response, td("June 9, 2019"))
        self.assertContains(response, td("April 21, 2020"))
        self.assertContains(response, td("New York City Authority"))
        self.assertContains(response, td("New York City"))
        self.assertContains(response, td("United States of America"))
        self.assertContains(response, "<td>testfile")
        self.assertContains(
            response, reverse("document_update", kwargs={"document_id": document.pk})
        )
        self.assertContains(
            response, reverse("document_delete", kwargs={"document_id": document.pk})
        )


def td(value):
    return "<td>" + str(value) + "</td>"
