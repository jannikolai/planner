from django.test import Client, RequestFactory
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertRedirects, assertTemplateUsed

from ...models import Document
from ...views.document import DeleteView
from ..asserts import assert_requires_profile
from ..factories import DocumentFactory, ProfileFactory, UserFactory


@mark.django_db
def test_requires_profile(rf: RequestFactory):
    assert_requires_profile(rf, DeleteView)


def url_for(document_id):
    return reverse("document_delete", kwargs={"document_id": document_id})


@mark.django_db
def test_returns_403_when_document_doesnt_exist(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)

    # Act
    client.force_login(user)
    response = client.get(url_for(42))

    # Assert
    assert response.status_code == 403


@mark.django_db
def test_renders_with_document_html(client: Client):
    # Arrange
    user = UserFactory()
    profile = ProfileFactory(user=user)

    DocumentFactory(id=91, profile=profile)

    # Act
    client.force_login(user)
    response = client.get(url_for(91))

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "confirm-delete.html")


@mark.django_db
def test_deletes_document(client: Client):
    # Arrange
    user = UserFactory()
    profile = ProfileFactory(user=user)

    DocumentFactory(id=481, profile=profile)

    # Act
    client.force_login(user)
    response = client.post(url_for(481), {})

    # Assert
    assertRedirects(response, reverse("document_list"))
    assert Document.objects.all().count() == 0


@mark.django_db
def test_returns_403_when_user_is_not_owner_of_document(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)

    DocumentFactory(id=194)

    # Act
    client.force_login(user)
    response = client.post(url_for(194), {})

    # Assert
    assert response.status_code == 403
