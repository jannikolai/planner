from django.test import Client
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertContains, assertTemplateUsed

from ...models import Answer, Question
from ..factories import AnswerFactory, ProfileFactory, QuestionFactory, UserFactory

url = reverse("question_answer")


@mark.django_db
def test_updates_answer_when_answer_exists(client: Client):
    # One of the migrations inserts data into the database...
    Question.objects.all().delete()

    # Arrange
    user = UserFactory()
    profile = ProfileFactory(user=user)
    question = QuestionFactory(id=174)
    AnswerFactory(text="Sample answer", profile=profile, question=question)

    # Act
    client.force_login(user)
    data = {"question174": "Some answer to a random question!"}
    response = client.post(url, data, follow=True)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")
    assert Answer.objects.filter().count() == 1
    assert Answer.objects.filter().first().text == "Some answer to a random question!"


@mark.django_db
def test_renders_with_form_html_when_profile_exists(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)

    # Act
    client.force_login(user)
    response = client.get(url)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")


@mark.django_db
def test_shows_existing_answer(client: Client):
    # Arrange
    user = UserFactory()
    profile = ProfileFactory(user=user)
    AnswerFactory(text="topSecretAnswer", profile=profile)

    # Act
    client.force_login(user)
    response = client.get(url)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")
    assertContains(response, "topSecretAnswer")


@mark.django_db
def test_doesnt_create_answer_when_question_is_not_mandatory_and_answer_is_blank(
    client: Client,
):
    # One of the migrations inserts data into the database...
    Question.objects.all().delete()

    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)
    QuestionFactory(mandatory=False)

    # Act
    client.force_login(user)
    response = client.post(url, {}, follow=True)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")
    assert Answer.objects.filter().count() == 0


@mark.django_db
def test_doesnt_update_answer_when_question_is_mandatory_and_answer_is_blank(
    client: Client,
):
    # One of the migrations inserts data into the database...
    Question.objects.all().delete()

    # Arrange
    user = UserFactory()
    profile = ProfileFactory(user=user)
    AnswerFactory(text="Example Answer", profile=profile, question__mandatory=True)

    # Act
    client.force_login(user)
    response = client.post(url, {})

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")
    assert Answer.objects.filter().count() == 1
    assert Answer.objects.filter().first().text == "Example Answer"


@mark.django_db
def test_doesnt_add_answer_when_question_is_mandatory_and_answer_is_blank(
    client: Client,
):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)

    # Act
    client.force_login(user)
    response = client.post(url, {})

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")
    assert Answer.objects.filter().count() == 0


@mark.django_db
def test_removes_optional_answer_when_no_answer_for_question_is_sent(client: Client):
    # One of the migrations inserts data into the database...
    Question.objects.all().delete()

    # Arrange
    user = UserFactory()
    profile = ProfileFactory(user=user)
    question_required = QuestionFactory(id=91, mandatory=True)
    question_optional = QuestionFactory(text="Question Optional", mandatory=False)
    AnswerFactory(text="Answer required", question=question_required, profile=profile)
    AnswerFactory(text="Answer optional", question=question_optional, profile=profile)

    # Act
    client.force_login(user)
    response = client.post(url, {"question91": "Answer required new"}, follow=True)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")
    assert Answer.objects.all().count() == 1
    assert Answer.objects.all().first().text == "Answer required new"


@mark.django_db
def test_renders_success_when_question_is_answered(client: Client):
    # One of the migrations inserts data into the database...
    Question.objects.all().delete()

    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)
    QuestionFactory(id=82)

    # Act
    client.force_login(user)
    data = {"question82": "Some answer to a random question!"}
    response = client.post(url, data, follow=True)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")
    assertContains(response, "alert-success")
    assert len(Answer.objects.all()) == 1
