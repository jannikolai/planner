from django.test import Client, RequestFactory
from django.urls import reverse
from pytest import mark
from pytest_django.asserts import assertContains, assertRedirects, assertTemplateUsed

from ...views.position import PositionUpdateView
from ..asserts import assert_requires_profile
from ..factories import PositionFactory, ProfileFactory, UserFactory


@mark.django_db
def test_requires_profile(rf: RequestFactory):
    assert_requires_profile(rf, PositionUpdateView)


url = reverse("requested_position_update")


@mark.django_db
def test_renders_with_form_html_when_profile_exists(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user)

    # Act
    client.force_login(user)
    response = client.get(url)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")


@mark.django_db
def test_shows_selected_positions_when_requested_positions_exists(client: Client):
    # Arrange
    user = UserFactory()
    ProfileFactory(user=user, requested_positions=[PositionFactory()])

    # Act
    client.force_login(user)
    response = client.get(url)

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")
    assertContains(
        response, 'checked="" class="form-check-input" id="id_requested_positions_'
    )


@mark.django_db
def test_renders_error_when_no_position_is_selected(client: Client):
    # Arrange
    user = UserFactory()
    profile = ProfileFactory(user=user, requested_positions=[PositionFactory()])

    # Act
    client.force_login(user)
    response = client.post(url, {})

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "form.html")
    assertContains(response, "alert-danger")
    assert len(profile.skills.all()) == 0


@mark.django_db
def test_redirects_to_questions(client: Client):
    # Arrange
    user = UserFactory()
    position = PositionFactory()
    profile = ProfileFactory(user=user, requested_positions=[position])

    # Act
    client.force_login(user)
    response = client.post(
        url + "?initial_registration", {"requested_positions": position.id},
    )

    # Assert
    assertRedirects(
        response, expected_url=f"{reverse('question_answer')}?initial_registration=yes"
    )
    assert profile.requested_positions.count() == 1
