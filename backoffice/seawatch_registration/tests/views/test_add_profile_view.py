from datetime import date

from django.core.exceptions import PermissionDenied
from django.test import Client
from django.urls import reverse
from pytest import mark, raises
from pytest_django.asserts import assertTemplateUsed

from ...models import Profile
from ..factories import UserFactory

url = reverse("profile_create")


@mark.django_db
@mark.xfail(reason="TODO: this needs to be fixed")
def test_disallows_creating_profiles_for_other_users(client: Client):
    client.force_login(UserFactory())

    with raises(PermissionDenied):
        client.post(
            url,
            {
                "user": UserFactory().id,
                "citizenship": "DE",
                "date_of_birth": date(2019, 7, 13),
                "place_of_birth": "New York",
                "country_of_birth": "US",
                "gender": "m",
                "needs_schengen_visa": False,
                "phone": "0123456789",
            },
        )


@mark.django_db
def test_redirects_when_form_is_valid(client: Client):
    # Arrange
    user = UserFactory()
    client.force_login(user)

    # Act
    response = client.post(
        url,
        {
            "user": UserFactory().id,
            "citizenship": "DE",
            "date_of_birth": date(2019, 7, 13),
            "place_of_birth": "New York",
            "country_of_birth": "US",
            "gender": "m",
            "needs_schengen_visa": False,
            "phone": "0123456789",
        },
    )

    # Assert
    assert response.status_code == 302
    assert Profile.objects.all().count() == 1


@mark.django_db
def test_doesnt_redirect_when_form_is_invalid(client: Client):
    # Arrange
    user = UserFactory()
    client.force_login(user)

    # Act
    response = client.post(url, {"user": user.id, "citizenship": "Deutsch"})

    # Assert
    assert response.status_code == 200
    assertTemplateUsed(response, "./seawatch_registration/profile.html")
    assert Profile.objects.all().count() == 0
