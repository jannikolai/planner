import factory
from faker.providers import BaseProvider


class QuestionProvider(BaseProvider):
    def question(self):
        return factory.Faker("sentence").generate()[:-1] + "?"


factory.Faker.add_provider(QuestionProvider)
