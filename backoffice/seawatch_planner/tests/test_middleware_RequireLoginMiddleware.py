from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import PermissionDenied
from django.shortcuts import reverse
from pytest import mark, raises

from seawatch_registration.tests.factories import UserFactory

from ..middleware import RequireLoginMiddleware

dummyResponse = object()


def respondStub(request):
    return dummyResponse


def test_denies_by_default(rf):
    request = rf.get("/any/random/path")
    request.user = AnonymousUser()

    response = RequireLoginMiddleware(respondStub, exceptions=[])(request)

    assert response.status_code == 302
    assert response.url == f"{reverse('login')}?next=/any/random/path"


@mark.django_db
def test_allow_when_logged_in(rf):
    request = rf.get("/any/random/path")
    request.user = UserFactory()

    response = RequireLoginMiddleware(respondStub, exceptions=[])(request)

    assert response is dummyResponse


@mark.django_db
def test_allows_exceptions(rf):
    request = rf.get("/special/path")
    request.user = AnonymousUser()

    response = RequireLoginMiddleware(respondStub, exceptions=[r"/special"])(request)

    assert response is dummyResponse
