from bs4 import BeautifulSoup
from django.http import HttpResponseRedirect
from django.shortcuts import reverse
from django.test import Client
from pytest import mark

from seawatch_registration.tests.factories import UserFactory


def test_requires_permission_by_default(client: Client):
    response = client.get("/this/path/doesnt/exist")
    assert response.status_code == 302
    assert response.url == f'{reverse("login")}?next=/this/path/doesnt/exist'


@mark.django_db
def test_login_possible(client: Client):
    user = UserFactory.build(username="beepboop")
    user.set_password("my voice is my passport")
    user.save()

    login_page = client.get(f"{reverse('login')}?next=/foo")
    assert login_page.status_code == 200
    html = BeautifulSoup(login_page.content, features="html.parser")
    assert html.find("input", attrs={"name": "username"})
    assert html.find("input", attrs={"name": "password"})

    login_response = client.post(
        f"{reverse('login')}?next=/foo",
        {"username": "beepboop", "password": "my voice is my passport"},
    )
    assert isinstance(login_response, HttpResponseRedirect)
    assert login_response.url == "/foo"

    dashboard = client.get("/")
    assert "beepboop" in dashboard.rendered_content
