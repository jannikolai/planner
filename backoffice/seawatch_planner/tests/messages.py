class StubMessageStorage:
    def __init__(self):
        self.messages = []

    def add(self, level, message, extra_tags=""):
        self.messages.append((level, message, extra_tags))
