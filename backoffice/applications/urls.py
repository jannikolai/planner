from django.urls import path  # type: ignore

from . import views

urlpatterns = [
    path("", views.ApplicationListView.as_view(), name="application_list"),
    path("<int:pk>", views.ApplicationDetailView.as_view(), name="application_detail"),
    path("import", views.ImportApplicationView.as_view(), name="application_import"),
    path(
        "<int:pk>/delete",
        views.ApplicationDeleteView.as_view(),
        name="application_delete",
    ),
    path("<int:pk>/accept", views.accept_application, name="application_accept"),
    path("volunteers", views.VolunteerListView.as_view(), name="volunteer_list"),
]
