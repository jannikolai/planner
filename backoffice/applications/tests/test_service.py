import uuid
from datetime import datetime
from urllib.request import Request

from applications.service import import_application
from django.conf import settings
from pytest_httpserver import HTTPServer


def test_import_with_mandatory_fields(httpserver: HTTPServer):
    # given
    settings.PLANNER_PUBLIC_BACKEND_URL = httpserver.url_for("")
    settings.PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = "insecure-preshared-token-for-dev"

    # a default response from public-backend
    httpserver.expect_request("/applications/oldest").respond_with_json(
        {
            "uid": str(uuid.uuid1()),
            "created": str(datetime.now()),
            "data": {
                "first_name": "Harry P.",
                "phone_number": "+3914568179",
                "spoken_languages": [{"language": "deu", "points": 3}],
            },
        }
    )

    # when we import latest applicants
    applicant = import_application()

    # latest applicant should be correct
    assert applicant.name == "Harry P."


def test_import_with_languages(httpserver: HTTPServer):
    # given
    settings.PLANNER_PUBLIC_BACKEND_URL = httpserver.url_for("")
    settings.PLANNER_PUBLIC_BACKEND_AUTH_TOKEN = "insecure-preshared-token-for-dev"

    # a default response from public-backend
    httpserver.expect_request("/applications/oldest").respond_with_json(
        {
            "uid": str(uuid.uuid1()),
            "created": str(datetime.now()),
            "data": {
                "first_name": "Harry P.",
                "phone_number": "+3914568179",
                "spoken_languages": [{"language": "deu", "points": "2"}],
            },
        }
    )

    # when we import latest applicants
    applicant = import_application()

    # languages should be present
    assert applicant.spoken_languages[0].code == "deu"
    assert applicant.spoken_languages[0].points == "2"
