from datetime import timezone
from uuid import uuid4

from factory import (
    DjangoModelFactory,
    Factory,
    Faker,
    LazyFunction,
    SubFactory,
    post_generation,
)
from faker.providers import BaseProvider
from seawatch_registration.tests.factories import PositionFactory

from ..models import (
    Application,
    ImportedApplication,
    ImportedApplicationDTO,
    ImportedLanguage,
    Volunteer,
)
from ..service import Language


class ApplicationFactory(DjangoModelFactory):
    class Meta:
        model = Application

    name = Faker("name")
    phone_number = Faker("bothify", text="+############")
    email = Faker("ascii_email")

    @post_generation
    def requested_positions(obj, create, positions, **kwargs):
        if create:
            if positions is None:
                positions = PositionFactory.create_batch(
                    Faker("random_int", min=1, max=5).generate()
                )
            obj.requested_positions.set(positions)


class LanguageProvider(BaseProvider):
    def language(self):
        return Language(code=self.language_code(), points=self.random_int(min=0, max=3))


Faker.add_provider(LanguageProvider)


class ImportedApplicationFactory(DjangoModelFactory):
    class Meta:
        model = ImportedApplication

    phone_number = Faker("bothify", text="+############")
    name = Faker("name")
    uid = LazyFunction(lambda: str(uuid4()))
    created = Faker("past_datetime", tzinfo=timezone.utc)


class ImportedLanguageFactory(DjangoModelFactory):
    class Meta:
        model = ImportedLanguage

    code = Faker("language_code")
    points = Faker("random_int", min=0, max=3)
    application = SubFactory(ImportedApplicationFactory)


class ImportedApplicationDTOFactory(Factory):
    class Meta:
        model = ImportedApplicationDTO

    uid = LazyFunction(lambda: str(uuid4()))
    created = Faker("past_datetime", tzinfo=timezone.utc)
    name = Faker("name")
    phone_number = Faker("bothify", text="+############")
    spoken_languages = Faker("pylist", value_types=Language, nb_elements=3)


class VolunteerFactory(DjangoModelFactory):
    class Meta:
        model = Volunteer

    name = Faker("name")
    phone_number = Faker("bothify", text="+############")
    email = Faker("ascii_email")
