from django.db import models
from phonenumber_field.modelfields import PhoneNumberField
from seawatch_registration.models import Position

from .service import ImportedApplicationDTO


class Application(models.Model):
    name = models.CharField(max_length=255)
    phone_number = PhoneNumberField()
    requested_positions = models.ManyToManyField(Position)
    # TODO: Propper Email field
    email = models.CharField(max_length=255)

    @classmethod
    def from_imported_application(cls, imported_application):
        return cls(
            name=imported_application.name,
            phone_number=imported_application.phone_number,
        )

    def __str__(self):
        return self.name


class Language(models.Model):
    code = models.CharField(max_length=3)
    points = models.IntegerField()

    application = models.ForeignKey(
        Application, on_delete=models.CASCADE, related_name="languages"
    )


class ImportedApplication(models.Model):
    uid = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    phone_number = PhoneNumberField()
    created = models.DateTimeField(editable=False)

    @classmethod
    def from_data_transfer_object(cls, dto: ImportedApplicationDTO):
        return cls(
            name=dto.name,
            uid=dto.uid,
            created=dto.created,
            phone_number=dto.phone_number,
        )


class ImportedLanguage(models.Model):
    code = models.CharField(max_length=3)
    points = models.IntegerField()

    application = models.ForeignKey(
        ImportedApplication, on_delete=models.CASCADE, related_name="languages"
    )


class Volunteer(models.Model):
    name = models.CharField(max_length=255)
    phone_number = PhoneNumberField()
    # TODO: Propper Email field
    email = models.CharField(max_length=255)

    def __str__(self):
        return self.name
