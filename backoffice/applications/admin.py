from django.contrib import admin

from . import models


@admin.register(models.Application)
class ApplicationAdmin(admin.ModelAdmin):
    pass


@admin.register(models.Volunteer)
class VolunteerAdmin(admin.ModelAdmin):
    pass


@admin.register(models.ImportedApplication)
class ImportedApplication(admin.ModelAdmin):
    readonly_fields = ('created',)
