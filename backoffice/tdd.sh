#!/bin/bash

if [ -z ${VIRTUAL_ENV+x} ]; then
    echo "tdd.sh must be run in the virtual env" >&2
else
    echo 'Executing `ptw -c -- --testmon`'
    exec ptw -c -- --testmon
fi
