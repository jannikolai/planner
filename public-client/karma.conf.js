/* eslint-disable import/no-extraneous-dependencies */
const { createDefaultConfig } = require("@open-wc/testing-karma");
const merge = require("deepmerge");

process.env.CHROME_BIN = require("puppeteer").executablePath();

module.exports = (config) => {
  config.set({
    browsers: ["ChromeHeadlessNoSandbox"],
    customLaunchers: {
      ChromeHeadlessNoSandbox: {
        base: "ChromeHeadless",
        flags: ["--no-sandbox", "--disable-setuid-sandbox"],
      },
    },

    frameworks: ["mocha", "source-map-support"],
    files: [
      {
        pattern: "test/**/*.test.js",
        watched: false,
      },
    ],
    preprocessors: {
      "test/**/*.test.js": ["webpack"],
    },
    coverageIstanbulReporter: {
      // TODO: Remove this once we have proper test coverage
      // Notes: I (Simon) am personally sceptical of the usefulness of this. 80% (the default)
      // allows us to ignore the "hard to test" 20% (a.k.a where bugs will happen), while 100% is
      // usually not realistic.
      thresholds: {
        global: {
          statements: 0,
          branches: 0,
          functions: 0,
          lines: 0,
        },
      },
    },
    autoWatch: false,
    singleRun: true,
    webpack: {
      mode: "development",
      plugins: [],
      ...require("./webpack.config")("test"),
    },
  });
  // Uncomment the next line to inspect the config
  // console.log(config);
  return config;
};
