import "@babel/polyfill/noConflict";

import initialSchema from 'common/schema/initialApplication'

import {fixture, expect} from "@open-wc/testing";
import initial_schema from "../../common/schema/initialApplication";

describe("Client schema", () => {
    it('Specifies data-validate to contain "required" for every required field', async () => {

        const formHandler = await fixture("<form-handler></form-handler>");

        initialSchema.required.forEach(name => {
            const elements = formHandler.shadowRoot.querySelectorAll(`[name="${name}"]`);
            expect(elements).to.have.length(1);

            const element = elements[0];
            const rules = element.dataset.validation.split(',');
            if (['date-picker', 'textarea-counter'].includes(element.tagName.toLowerCase())) {
                expect(rules).to.contain('required');
            } else if (element.tagName.includes('-')) { // it's a web component
                expect(rules).to.contain('minOneEntry');
            } else {
                expect(rules).to.contain('required');
            }
        })
    });

    it('requires one input element and at least one label for each field', async () => {
        const formHandler = await fixture("<form-handler></form-handler>");

        const unusedInputs = [...formHandler.shadowRoot.querySelectorAll('[name]')].map(el => el.getAttribute('name'));
        const keys = Object.keys(initial_schema.properties);
        keys.forEach(key => {
            const inputs = formHandler.shadowRoot.querySelectorAll(`[name="${key}"]`);
            expect(inputs).to.have.length(1, `No input ${key}`);

            const labels = formHandler.shadowRoot.querySelectorAll(`[for="${key}"]`);
            expect(labels).to.have.length.greaterThan(0, `No label for ${key}`)

            unusedInputs.splice(unusedInputs.indexOf(key), 1);

        });
        expect(unusedInputs).to.be.empty;
    });
});
