import "@babel/polyfill/noConflict";

import {expect} from "@open-wc/testing";
import validate from "../../src/js/app/elements/FormHandler/validator";

describe("Validator", () => {
    it('accepts valid input', async () => {
        const elements = [
            {name: 'myemail', rules: ['email']}
        ];
        const data = {myemail: 'ab@cd.com'};

        const errors = validate(elements, data);
        expect(errors).to.be.empty;
    });

    it('returns errors for invalid input', async () => {
        const elements = [
            {name: 'myemail', rules: ['email']}
        ];
        const data = {myemail: 'nope'};

        const errors = validate(elements, data);
        expect(errors).to.have.length(1);

        const {name, error} = errors[0];
        expect(name).to.equal('myemail');
        expect(error).to.contain('does not look like an email');
    });
});
