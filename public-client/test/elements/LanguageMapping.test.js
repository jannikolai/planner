import "@babel/polyfill/noConflict";

import { oneEvent, expect, fixture, nextFrame, triggerFocusFor } from "@open-wc/testing";

import "../../src/js/app/elements/LanguageMapping/index.js";

function getContainer(shadowRoot) {
  const children = shadowRoot.children;
  for (var i = 0; i < children.length; i++) {
    const child = children[i];
    if (child.className == "container") {
      return child;
    }
  }
  throw "Container not found";
}

describe("LanguageMapping", () => {
  it("contains a select element", async () => {
    const el = await fixture(`<language-mapping></language-mapping>`);
    const container = getContainer(el.shadowRoot);
    expect(container).to.contain("select");
  });
  it("contains some of the requested languages", async () => {
    const el = await fixture(`<language-mapping></language-mapping>`);
    const container = getContainer(el.shadowRoot);
    [
      "English",
      "French",
      "Italian",
      "German",
      "Spanish",
      "Bengali",
      "Tigrinya",
      "Somali",
    ].forEach((language) => {
      expect(container).to.contain.text(language);
    });
  });
  it('supports "Standard Arabic" and "Colloquial Arabic" (TODO!)');
  it("is accessible (whatever that means)", async () => {
    const el = await fixture(`<language-mapping></language-mapping>`);
    expect(el).shadowDom.to.be.accessible;
  });
  it("sends a change event when a choice is added", async () => {
    const el = await fixture(`<language-mapping></language-mapping>`);
    const listener = oneEvent(el, "change");
    el.choices.setChoiceByValue("deu");
    const { detail, bubbles } = await listener;
    expect(detail).to.deep.equal([ { language: 'deu', points: 0 } ] );
    expect(bubbles).to.be.true;
  });
  it("the language choices are opened when the component receives focus", async () => {
    const el = await fixture(`<language-mapping></language-mapping>`);

    const input = el.shadowRoot.querySelector('input');
    expect(input).to.be.accessible;

    await triggerFocusFor(el);

    await nextFrame();
    const choicesClassList = Array.from(el.shadowRoot.querySelector('.choices').classList);
    expect(choicesClassList).to.contain('is-open');
    expect(el.shadowRoot.activeElement).to.equal(input);
  });
});
