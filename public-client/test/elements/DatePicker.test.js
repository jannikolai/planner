import "@babel/polyfill/noConflict";

import {expect, fixture, nextFrame,triggerFocusFor} from "@open-wc/testing";

import "../../src/js/app/elements/DatePicker/index.js";
import {getMaxDay} from "../../src/js/app/elements/DatePicker";

describe("DatePicker", () => {
  it("contains three required selects when required is set", async () => {
    const el = await fixture(
      `<date-picker required></date-picker>`
    );
    const dropdowns = el.shadowRoot.querySelectorAll("select");
    expect(dropdowns[0]).to.have.attribute("required");
    expect(dropdowns[1]).to.have.attribute("required");
    expect(dropdowns[2]).to.have.attribute("required");
  });

  it("focuses the first dropdown on focus", async () => {
    const el = await fixture(`<date-picker></date-picker>`);
    const dropdowns = el.shadowRoot.querySelectorAll("select");
    expect(dropdowns[0]).to.be.accessible;

    await triggerFocusFor(el);

    await nextFrame();
    expect(el.shadowRoot.activeElement).to.equal(dropdowns[0]);
  });

  it("will accept a valid date", async () => {
    const el = await fixture(`<date-picker></date-picker>`);

    const dropdowns = el.shadowRoot.querySelectorAll("select");
    expect(el.value).to.equal('');

    dropdowns[0].value = '1999';
    dropdowns[0].dispatchEvent(new Event('change', {bubbles: true}));
    dropdowns[1].value = '12';
    dropdowns[1].dispatchEvent(new Event('change', {bubbles: true}));
    dropdowns[2].value = '31';
    dropdowns[2].dispatchEvent(new Event('change', {bubbles: true}));

    expect(el.value).to.equal('1999-12-31');
  });

  it("wont accept a date in the current year or in the future", async () => {
    const el = await fixture(`<date-picker></date-picker>`);

    const lastYear = (new Date().getFullYear() - 1).toString();
    const currentYear = new Date().getFullYear().toString();

    // make it valid fist
    const dropdowns = el.shadowRoot.querySelectorAll("select");
    dropdowns[0].value = lastYear;
    dropdowns[0].dispatchEvent(new Event('change', {bubbles: true}));
    dropdowns[1].value = '1';
    dropdowns[1].dispatchEvent(new Event('change', {bubbles: true}));
    dropdowns[2].value = '2';
    dropdowns[2].dispatchEvent(new Event('change', {bubbles: true}));
    expect(el.value).to.equal(`${lastYear}-01-02`);

    // actual test
    dropdowns[0].value = currentYear.toString();
    dropdowns[0].dispatchEvent(new Event('change', {bubbles: true}));
    expect(el.value).to.equal('');

    dropdowns[0].value = '2525';
    dropdowns[0].dispatchEvent(new Event('change', {bubbles: true}));
    expect(el.value).to.equal('');
  });

  it("wont accept a date too far in the past", async () => {
    const el = await fixture(`<date-picker></date-picker>`);

    // make it valid fist
    const dropdowns = el.shadowRoot.querySelectorAll("select");
    dropdowns[0].value = '1999';
    dropdowns[0].dispatchEvent(new Event('change', {bubbles: true}));
    dropdowns[1].value = '12';
    dropdowns[1].dispatchEvent(new Event('change', {bubbles: true}));
    dropdowns[2].value = '31';
    dropdowns[2].dispatchEvent(new Event('change', {bubbles: true}));
    expect(el.value).to.equal('1999-12-31');

    // actual test
    dropdowns[0].value = '1800';
    dropdowns[0].dispatchEvent(new Event('change', {bubbles: true}));
    dropdowns[1].value = '01';
    dropdowns[1].dispatchEvent(new Event('change', {bubbles: true}));
    dropdowns[2].value = '01';
    dropdowns[2].dispatchEvent(new Event('change', {bubbles: true}));

    expect(el.value).to.equal('');
  });

  it("returns an empty value until year, month and day are set", async () => {
    const el = await fixture(`<date-picker></date-picker>`);
    const dropdowns = el.shadowRoot.querySelectorAll("select");

    dropdowns[0].value = '1999';
    dropdowns[0].dispatchEvent(new Event('change', {bubbles: true}));
    expect(el.value).to.equal('');

    dropdowns[1].value = '12';
    dropdowns[1].dispatchEvent(new Event('change', {bubbles: true}));
    expect(el.value).to.equal('');

    dropdowns[2].value = '31';
    dropdowns[2].dispatchEvent(new Event('change', {bubbles: true}));
    expect(el.value).to.equal('1999-12-31');
  });

  it("returns an empty value again if the day is invalidated", async () => {
    const el = await fixture(`<date-picker></date-picker>`);

    const dropdowns = el.shadowRoot.querySelectorAll("select");

    dropdowns[0].value = '1999';
    dropdowns[0].dispatchEvent(new Event('change', {bubbles: true}));
    expect(el.value).to.equal('');

    dropdowns[1].value = '12';
    dropdowns[1].dispatchEvent(new Event('change', {bubbles: true}));
    expect(el.value).to.equal('');

    dropdowns[2].value = '31';
    dropdowns[2].dispatchEvent(new Event('change', {bubbles: true}));
    expect(el.value).to.equal('1999-12-31');

    dropdowns[1].value = '2';
    dropdowns[1].dispatchEvent(new Event('change', {bubbles: true}));
    expect(el.value).to.equal('');
  });

  it("getMaxDay is able to figure out leap years", async () => {
    // 2020 is a leap year
    expect(getMaxDay(2020,1)).to.equal(31);
    expect(getMaxDay(2020,2)).to.equal(29);
    expect(getMaxDay(2020,3)).to.equal(31);
    expect(getMaxDay(2020,4)).to.equal(30);
    expect(getMaxDay(2020,5)).to.equal(31);
    expect(getMaxDay(2020,6)).to.equal(30);
    expect(getMaxDay(2020,7)).to.equal(31);
    expect(getMaxDay(2020,8)).to.equal(31);
    expect(getMaxDay(2020,9)).to.equal(30);
    expect(getMaxDay(2020,10)).to.equal(31);
    expect(getMaxDay(2020,11)).to.equal(30);
    expect(getMaxDay(2020,12)).to.equal(31);

    // 2021 is not
    expect(getMaxDay(2021,2)).to.equal(28);
  });
});
