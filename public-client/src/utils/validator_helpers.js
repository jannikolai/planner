export default {
    required: function (str = null) {
        const isBlank = (!str || /^\s*$/.test(str));
        if (isBlank) {
            return 'This field is required'
        }
    },
    minOneEntry: function (array) {
        const isValid = Array.isArray(array) && array.length >= 1;
        if (!isValid) {
            return 'Minimum one entry is required'
        }
    },
    email: function (email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        const isValid = re.test(String(email).toLowerCase());
        if (!isValid) {
            return 'This does not look like an email address'
        }
    },
    isodate: function (str) {
        const re = /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/;
        const isValid = re.test(String(str));
        if (!isValid) {
            return 'Please use a format like 1999-12-31'
        }
    }
}
