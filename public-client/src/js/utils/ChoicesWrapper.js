import Choices from "choices.js";

export default class ChoicesWrapper extends Choices {
  constructor(
    element = "[data-choice]",
    userConfig = {},
    doc = document.documentElement
  ) {
    super(element, userConfig);
    this.doc = doc;
    this._addEventListeners();

    // Prevent invisible dropdown to become focusable through tabbing in firefox
    this.choiceList.element.tabIndex = -1;

    // Hide the placeholder when at least one option is selected
    // Taken from https://github.com/jshjohnson/Choices/issues/770
    this.passedElement.element.addEventListener("removeItem", (e) => {
      if (this.getValue(true).length < 1) {
        const input = this.input.element;
        input.classList.remove("choices__input--cloned--placeholder--hidden");
      }
    });
    this.passedElement.element.addEventListener("addItem", (e) => {
      const input = this.input.element;
      input.classList.add("choices__input--cloned--placeholder--hidden");
    });
  }

  _addEventListeners() {
    if (!this.doc) return;

    const documentElement = this.doc;

    // capture events - can cancel event processing or propagation
    documentElement.addEventListener("touchend", this._onTouchEnd, true);
    this.containerOuter.element.addEventListener(
      "keydown",
      this._onKeyDown,
      true
    );
    this.containerOuter.element.addEventListener(
      "mousedown",
      this._onMouseDown,
      true
    );

    // passive events - doesn't call `preventDefault` or `stopPropagation`
    documentElement.addEventListener("click", this._onClick, { passive: true });
    documentElement.addEventListener("touchmove", this._onTouchMove, {
      passive: true,
    });
    this.dropdown.element.addEventListener("mouseover", this._onMouseOver, {
      passive: true,
    });

    if (this._isSelectOneElement) {
      this.containerOuter.element.addEventListener("focus", this._onFocus, {
        passive: true,
      });
      this.containerOuter.element.addEventListener("blur", this._onBlur, {
        passive: true,
      });
    }

    this.input.element.addEventListener("keyup", this._onKeyUp, {
      passive: true,
    });

    this.input.element.addEventListener("focus", this._onFocus, {
      passive: true,
    });
    this.input.element.addEventListener("blur", this._onBlur, {
      passive: true,
    });

    if (this.input.element.form) {
      this.input.element.form.addEventListener("reset", this._onFormReset, {
        passive: true,
      });
    }

    this.input.addEventListeners();
  }

  _onAKey = ({ hasItems, hasCtrlDownKeyPressed }) => {
    // If CTRL + A or CMD + A have been pressed and there are items to select
    if (hasCtrlDownKeyPressed && hasItems) {
      this._canSearch = false;

      const shouldHightlightAll =
        this.config.removeItems &&
        !this.input.value &&
        (this.input.element === document.activeElement ||
          this.input.element === this.doc.activeElement);

      if (shouldHightlightAll) {
        this.highlightAll();
      }
    }
  };

  _onClick(e) {
    const { target } = e;
    const clickWasWithinContainer =
      target === this.passedElement.element ||
      this.containerOuter.element.contains(target);

    if (clickWasWithinContainer) {
      if (!this.dropdown.isActive && !this.containerOuter.isDisabled) {
        if (this._isTextElement) {
          if (this.doc.activeElement !== this.input.element) {
            this.input.focus();
          }
        } else {
          this.showDropdown();
          this.containerOuter.focus();
        }
      } else if (
        this._isSelectOneElement &&
        target !== this.input.element &&
        !this.dropdown.element.contains(target)
      ) {
        this.hideDropdown();
      }
    } else {
      const hasHighlightedItems = this._store.highlightedActiveItems.length > 0;

      if (hasHighlightedItems) {
        this.unhighlightAll();
      }

      this.containerOuter.removeFocusState();
      this.hideDropdown(true);
    }
  }
}
