
import css from '../css/base.css';

import App from './app/app.js';

const style = document.createElement('style');
style.appendChild(document.createTextNode(css));
document.head.appendChild(style);

function onReady(){
    const anchor = document.getElementById('anchor');
    const app = new App(anchor);
}

if (document.readyState !== "loading") {
    onReady();
} else {
    document.addEventListener("DOMContentLoaded", onReady);
}
