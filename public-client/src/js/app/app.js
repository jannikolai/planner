import "regenerator-runtime/runtime";

import './elements/LanguageMapping'
import './elements/FormHandler'
import './elements/MultiSelect'
import './elements/TextareaCounter'
import './elements/ValidationFeedback'
import './elements/DatePicker'
import '@polymer/paper-slider/paper-slider.js';

class App {
    constructor(anchor) {
        this.anchor = anchor;

        this.loadingAnchor = document.createElement('div');
        this.loadingAnchor.innerHTML = 'Loading';
        this.updateAnchor(this.loadingAnchor);

        this.start();
    }

    start() {
        this
            .getForm()
            .then(form => {
                console.log('App booted.', form);
                this.updateAnchor(form);
            })
            .catch(e => {
                console.error('App crashed: ', e);
                const msg = document.createElement('div');
                msg.innerHTML = e;
                this.updateAnchor(msg);
            });
    }

    async getForm() {
        return document.createElement('form-handler');
    }

    updateAnchor(htmlElement) {
        this.anchor.innerHTML = '';
        this.anchor.appendChild(htmlElement);
    }
}

export default App;
