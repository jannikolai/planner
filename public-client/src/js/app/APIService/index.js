import {GET_FORM_DATA, SEND_FORM_DATA, query} from "../config/json_interface";

export const getFormData = async () => {
    return await query(GET_FORM_DATA, {});
};

export const sendFormData = async (formData) => {
    return await query(SEND_FORM_DATA, {formData});
};
