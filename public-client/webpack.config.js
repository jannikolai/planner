const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const path = require("path");
const BundleTracker = require("webpack-bundle-tracker");
const { DefinePlugin } = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const postcssLoaderOptions = {
    ident: "postcss",
    plugins: [
      require("autoprefixer")({}),
      require("postcss-nesting")({}),
      require("postcss-selector-matches")({}),
      require("cssnano")({
        preset: [
          "default",
          { discardComments: { removeAll: true } },
        ],
      }),
    ],
    sourceMap: true,
};

module.exports = (env) => {
  const API_BASEURL = process.env.API_BASEURL || "/";
  return {
    resolve: {
      modules: ["src/js", "src", "node_modules"],
      alias: {
        common: path.resolve(__dirname, "..", "common"),
      },
    },
    entry: "./src/js/index.js",
    output: {
      path: __dirname + "/dist/compiled/",
      publicPath: "/static/compiled/",
      filename: "[hash].js",
    },
    plugins: [
      new CleanWebpackPlugin(),
      new MiniCssExtractPlugin({
        filename: "[hash].css",
      }),
      new BundleTracker({ filename: "./webpack-stats.json" }),
      new DefinePlugin({ API_BASEURL: JSON.stringify(API_BASEURL) }),
    ],
    devtool: "inline-source-map",
    module: {
      rules: [
        {
          test: /\.js$/,
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
        {
          test: /\.(svg|ttf|png)$/i,
          use: ["file-loader"],
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf)$/,
          use: ["file-loader"],
        },
        {
          test: /\.html$/,
          use: ["raw-loader"],
        },
          {
              test: /\.cssfrag$/,
              use: [

                  "raw-loader",
                  {
                      loader: "postcss-loader",
                      options: postcssLoaderOptions,
                  },],
          },
        {
          test: /\.s?css$/i,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
            },
            {
              loader: "css-loader",
              options: {
                url: true,
                importLoaders: 1,
                sourceMap: true,
              },
            },
            {
              loader: "postcss-loader",
              options: postcssLoaderOptions,
            },
          ],
        },
      ],
    },
  };
};
